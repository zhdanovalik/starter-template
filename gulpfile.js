'use strict';

var gulp = require('gulp');
var sourcemaps = require('gulp-sourcemaps');
var sass = require('gulp-sass');

var autoprefixer = require('gulp-autoprefixer');
var del = require('del');

var gutil = require('gulp-util');
var newer = require('gulp-newer');
var remember = require('gulp-remember');
var cache = require('gulp-cached');
var browserSync = require('browser-sync').create();
var reload =  browserSync.reload;
var changed = require('gulp-changed');
var plumber = require('gulp-plumber');
var notify = require("gulp-notify");

var svgSprite = require('gulp-svg-sprite');

var jade = require('gulp-jade');

var paths = {
		html: {
			src: 'app/templates/',
			dest: 'app/'
		},
    img: {
        src: 'app/img/for_sprite/',
        dest: 'app/img/sprite/sprite.svg'
    },
    scripts: {
        src: 'static/coffee/',
        dest: 'static/js/'
    },
    styles: {
        src: 'app/sass/',
        dest: 'app/css/'
    },
};

var appFiles = {
    styles: paths.styles.src + 'style.scss',
    stylesWatch: paths.styles.src + '**/*.scss',
    scripts: paths.scripts.src + 'js.coffee',
    scriptsDest: paths.scripts.src + 'js.js',
    images: paths.img.src + '*.svg',
		html: paths.html.src + '*.jade',
    htmlDest: paths.html.dest + '*.html'
};


gulp.task('styles', function() {
    return gulp.src(appFiles.styles)
        .pipe(plumber({errorHandler: notify.onError("Error: <%= error.message %>")}))
        .pipe(sourcemaps.init())
		.pipe(sass())
		.pipe(autoprefixer({browsers: ['> 1%', 'IE 9']}))
        .pipe(sourcemaps.write())
		.pipe(gulp.dest(paths.styles.dest))
        .pipe(browserSync.stream())
        .pipe(notify("Compiled"));
});

gulp.task('html', function() {
    return gulp.src(appFiles.html)
        .pipe(changed(paths.html.dest, {extension: '.html'}))
		.pipe(cache('html'))
		.pipe(jade({
            pretty: true
        }))
        .pipe(remember('html'))
		.pipe(gulp.dest(paths.html.dest));
});

gulp.task('svg', function() {
	return gulp.src('app/img/svg/*.svg')
	.pipe(svgSprite({
		mode: {
			symbol: {
				dest: '.',
				sprite: 'sprite.svg',
				prefix: 'icon-'
			}
	    }
	}))
	.pipe(gulp.dest('app/img/'));
});



gulp.task('watch',['styles', 'html'], function(){

    browserSync.init({
        server: "./app"
    });

    gulp.watch('app/sass/**/*', ['styles']);
		gulp.watch("app/templates/**/*", ['html']);
    gulp.watch("app/*.html").on('change', browserSync.reload);
		gulp.watch("app/js/*.js").on('change', browserSync.reload);
});

gulp.task('build',['scripts', 'styles'], function() {
    console.log('***build finished correct***');
});
gulp.task('default',['scripts', 'styles'], function() {
    console.log('***build finished correct***');
});
